# VPC with multiple Availability Zone support

This template sets up an AWS Virtual Private Cloud (VPC) with subnets in up to
three availability zones. Whenever a CIDR value is not provided for a subnet,
the subnet will not be created.

## Splitting up the subnets

Partition your allocated VPC CIDR block so that you have enough space in each 
subnet and each availability zone.  For example if we asked for a small /24 range:

Purpose           | CIDR             | # of IP Addresses
-------------------------------------------- 
VPC               | 10.0.200.0/24    | 256
Public Subnet A   | 10.0.200.0/27    | 32
Public Subnet B   | 10.0.200.32/27   | 32
Private Subnet A  | 10.0.200.64/26   | 64
Private Subnet B  | 10.0.200.128/26  | 64
Un-Allocated      | 10.0.200.192/26  | 64

Here we start with a /24 block of 256 IPs.  Since public subnets usually have fewer
resources in them (mostly just load balancers, NAT, bastion hosts) we give that a 
little less space, a /27 of 32 IPs.  For the private subnets we have two blocks of
/26 for 64 IPs each.  This leaves us with a block of 64 un-allocated which could be
used in the future. Unfortunately due to CIDR math, you can't split that remaining
64 in half and give it to the existing private subnets, but you could create two new
private subnets if you wanted.

## Campus connectivity

If it is desired to have campus connectivty for the VPC, follow the procedure documented at [https://emailarizona.sharepoint.com/:w:/r/sites/UITS-CloudOpenSystems/_layouts/15/doc2.aspx?sourcedoc=%7BD1647C07-D561-4A68-AD20-2505834DBB53%7D&file=Create%20a%20VPC%20with%20campus%20connectivity%20via%20Transit%20Gateway.docx&action=default&mobileredirect=true&cid=378de2bc-5812-4dce-95d0-5c6fb6c0053c](https://emailarizona.sharepoint.com/:w:/r/sites/UITS-CloudOpenSystems/_layouts/15/doc2.aspx?sourcedoc=%7BD1647C07-D561-4A68-AD20-2505834DBB53%7D&file=Create%20a%20VPC%20with%20campus%20connectivity%20via%20Transit%20Gateway.docx&action=default&mobileredirect=true&cid=378de2bc-5812-4dce-95d0-5c6fb6c0053c)